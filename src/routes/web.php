<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'EstudianteController@principal')->name('estudiante.principal');

Route::get('inscripciontema/create', 'InscripcionTemaController@create')->name('inscripcion-tema.create');
Route::post('inscripciontema', 'InscripcionTemaController@store')->name('inscripcion-tema.store');