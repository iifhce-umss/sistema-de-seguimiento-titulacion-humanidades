@extends('master')

@section('title', "Registro de tema")
    
@section('content')
<div class="container">
    <div aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/"><span class="oi oi-home"></span> Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Inscripción de tema</li>
        </ol>
    </div>
</div>


<div class="container">
    <h2 class="display-4 mb-3 mt-2">Inscripción de Tema</h2>
    <br>
    @if ($errors->any())
        <div class="alert alert-danger">
            Algunos campos tienen errores, corrija y vuelva a enviar por favor.
        </div>
    @endif
    <div class="card border-primary">
        <div class="card-body">
            <form action="{{route('inscripcion-tema.store')}}" method="POST">
                @csrf
            <!-- <h3> <span class="font-weight-bold">Inscripción</span> de tema de investigación (Tesis)</h3> -->
            <div class="alert alert-warning"> <span class="oi oi-warning"></span> Llene los campos con mucha atención.</div>
            <div class="row bg-lightgray text-white" style="height: 3.2em">
                <div id="titulo-subseccion-1" class="col bg-primary d-flex align-items-center justify-content-center">
                    <p class="m-0">Datos generales</p>
                </div>
                <div id="titulo-subseccion-2" class="col d-flex align-items-center justify-content-center">
                    <p class="m-0">Descripción del proyecto</p>
                </div>
            </div>
            <br>
            <fieldset id="set-datos-generales">
                <div class="form-group">
                    <label for="nombreTesista">Tesista</label>
                    <input type="text" class="form-control" id="nombreTesista" value="{{$datos->nombre}}" name="nombre_tesista" readonly>
                </div>
                <div class="form-group">
                    <label for="carrera">Carrera</label>
                    <input type="text" class="form-control" id="carrera" value="{{$datos->carrera}}" name="carrera" readonly>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="direccion">Dirección</label>
                        <input type="text" class="form-control @error('direccion') is-invalid @enderror" id="direccion" placeholder="Calle/Av, zona, etc." name="direccion">
                        @error('direccion')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group col">
                        <label for="telefono">Telefono/Celular</label>
                        <input type="phone" class="form-control @error('telefono') is-invalid @enderror" id="telefono" value="{{$datos->celular}}" name="telefono">
                        @error('telefono')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="emailEstudiante">E-mail</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{$datos->email}}" name="email">
                    @error('email')
                    <div class="invalid-feedback">
                        {{$message}}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="fechaSolicitud">Fecha de solicitud</label>
                    <input type="text" class="form-control" id="fechaSolicitud" name="fecha_solicitud" readonly>
                </div>
                <div class="row">
                    <div class="col d-flex justify-content-between">
                        <input type="button" class="btn btn-link" value="Cancelar inscripción">
                        <input type="button" class="btn btn-primary" value="Siguiente" id="btn-siguiente">
                    </div>
                </div>
            </fieldset>

            <fieldset id="set-descripcion-proyecto" class="d-none">
                <div class="form-group">
                    <p class="font-weight-bold">Linea de investigación</p>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="opcion1" name="linea_investigacion" class="custom-control-input @error('linea_investigacion') is-invalid @enderror" value="1">
                        <label class="custom-control-label" for="opcion1">Educación, interculturalidad y bilingüismo</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="opcion2" name="linea_investigacion" class="custom-control-input @error('linea_investigacion') is-invalid @enderror" value="2">
                        <label class="custom-control-label" for="opcion2">Procesos y actores en la Educación Superior</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="opcion3" name="linea_investigacion" class="custom-control-input @error('linea_investigacion') is-invalid @enderror" value="3">
                        <label class="custom-control-label" for="opcion3">Identidad y exclusión social (Salud mental y Calidad de vida)</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="opcion4" name="linea_investigacion" class="custom-control-input @error('linea_investigacion') is-invalid @enderror" value="4">
                        <label class="custom-control-label" for="opcion4">Procesos comunicacionales y lingüísticos</label>
                        @error('linea_investigacion')
                        <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="titulo">Título del tema de investigación</label>
                    <span class="oi oi-question-mark icono-circulo" data-toggle="tooltip" data-placement="top" title="Escriba el título en mayúsculas obligatoriamente." ></span>
                    <input type="text" class="form-control @error('titulo_tema') is-invalid @enderror" id="titulo" placeholder="ESCRIBA EN MAYÚSCULAS" name="titulo_tema">
                    @error('titulo_tema')
                    <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="titulo">Idioma del título</label>
                    <select class="form-control" id="idioma-titulo" name="idioma_titulo">
                        <option selected>Español</option>
                        <option>Quechua</option>
                        <option>Inglés</option>
                        <option>Francés</option>
                        <option>Otro</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="titulo-traducido">Título traducido</label>
                    <span class="oi oi-question-mark icono-circulo" data-toggle="tooltip" data-placement="top" title="Traducir el título a español, solo en caso que sea un idioma diferente."></span>
                    <input type="text" class="form-control" id="titulo-traducido" name="titulo_traducido" disabled>
                </div>
                <div class="form-group">
                    <label for="objetivo-general">Objetivo general</label>
                    <textarea class="form-control @error('objetivo_general') is-invalid @enderror" name="objetivo_general" id="objetivo-general" rows="3"></textarea>
                    @error('objetivo_general') 
                    <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="objetivos-especificos">Objetivos específicos</label>
                    <span class="oi oi-question-mark icono-circulo" data-toggle="popover" data-html="true" title="Ejemplo" data-content="<p>Objetivo específico 1 ... </p><p>Objetivo específico 2 ... </p><p> ... </p>"></span>
                    <textarea class="form-control @error('objetivos_especificos') is-invalid @enderror" name="objetivos_especificos" id="objetivos-especificos" rows="6" placeholder="Cada salto de linea cuenta como un objetivo. Vea el ejemplo haciendo click en el icono '?'"></textarea>
                    @error('objetivos_especificos')
                    <div class="invalid-feedback">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="extension-temporal">Extensión temporal</label>
                    <span class="oi oi-question-mark icono-circulo" data-toggle="tooltip" data-placement="top" title="Se refiere al lapso de tiempo previsto y necesario para ejecutar su proyecto."></span>
                    <textarea class="form-control @error('extension_temporal') is-invalid @enderror" name="extension_temporal" id="extension_temporal" rows="3" placeholder="Se refiere al lapso de tiempo previsto y necesario para ejecutar su proyecto."></textarea>
                     @error('extension_temporal') 
                     <div class="invalid-feedback">{{$message}}</div>
                     @enderror
                </div>
                <div class="form-group">
                    <label for="sintesis">Breve síntesis del proyecto</label>
                    <span class="oi oi-question-mark icono-circulo" data-toggle="tooltip" data-placement="top" title="Redacte brevemente lo que sugiere su tema de investigación, algunos referentes teóricos-conceptuales como posibles dificultades en el abordaje metodológico. Pueden incluirse también motivaciones personales."></span>
                    <textarea class="form-control @error('sintesis') is-invalid @enderror" name="sintesis" id="sintesis" rows="12"></textarea>
                     @error('sintesis')
                     <div class="invalid-feedback">Mensjae de error</div>
                     @enderror
                    <small class="form-text text-muted">Escriba mínimo 80 palabras y máximo 200 palabras.</small>
                </div>
                <div class="row">
                    <div class="col d-flex justify-content-between">
                        <input type="button" class="btn btn-link" value="Atras" id="btn-atras">
                        <input type="submit" class="btn btn-primary"  value="Registrar">
                    </div>
                </div>
            </fieldset>
            </form>
        </div>
    </div>

</div>
@endsection

@section('scripts')
<script>
    $(function() {
        var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        var f=new Date();
        const fechaActual = "{{$datos->fecha_actual['mday']}}" + " de " + meses["{{$datos->fecha_actual['mon']}}"-1] + " de " + "{{$datos->fecha_actual['year']}}";
        $('#fechaSolicitud').val(fechaActual)

        $('#btn-siguiente').on('click', function(e){
            e.preventDefault();
            $('#set-datos-generales').addClass('d-none');
            $('#set-descripcion-proyecto').removeClass('d-none');

            $('#titulo-subseccion-1').removeClass('bg-primary');
            $('#titulo-subseccion-2').addClass('bg-primary');
        })

        $('#btn-atras').on('click', function(e){
            e.preventDefault(e);
            $('#set-datos-generales').removeClass('d-none');
            $('#set-descripcion-proyecto').addClass('d-none');

            $('#titulo-subseccion-1').addClass('bg-primary');
            $('#titulo-subseccion-2').removeClass('bg-primary');
        })

        $('#idioma-titulo').on('change', function(){
            let idioma = $(this).val();
            if(idioma === 'Otro'){
                //queda pendiente para implementar cuando no es un idioma de la lista.
                console.log('se selecciono: '+idioma);
            } 
            
            if(idioma === 'Español'){
                $('#titulo-traducido').attr('disabled', true)
            } else {
                $('#titulo-traducido').attr('disabled', false)
            }

        })
    });
</script>
@endsection