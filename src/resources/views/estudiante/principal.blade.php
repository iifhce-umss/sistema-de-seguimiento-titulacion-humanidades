@extends('master')

@section('title', "Panel principal")

@section('content')    
    <div class="container">
        <h2 class="display-4 mb-3 mt-2">Inicio</h2>

        <div class="datos-estudiante card">
            <div class="card-body">
                <h4 class="card-title mb-4">Datos de estudiante</h4>
                
                <div class="row">
                    <div class="col-4 font-weight-bold">Estudiante:</div><div class="col-7">{{$estudiante->nombre}}</div>
                </div>
                <div class="row">
                    <div class="col-4 font-weight-bold">Carrera actual:</div><div class="col-7">{{$estudiante->carrera_actual}}</div>
                </div>
                <div class="row">
                    <div class="col-4 font-weight-bold">Materias inscritas:</div><div class="col-7">@foreach ($estudiante->materias_inscritas as $mat) <p class="m-0"> {{$mat}} </p> @endforeach</div>
                </div>
                <div class="row">
                    <div class="col-4 font-weight-bold">Email:</div><div class="col-7">{{$estudiante->email}}</div>
                </div>
                <div class="row">
                    <div class="col-4 font-weight-bold">Celular:</div><div class="col-7">{{$estudiante->celular}}</div>
                </div>

            </div>
        </div>
    </div>

@endsection

</body>
</html>