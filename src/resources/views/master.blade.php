<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SST - @yield('title')</title>
    <link rel="stylesheet" href="{{asset('bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('open-iconic/font/css/open-iconic-bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
</head>
<body>
    <nav class="menu-principal bg-dark-primary">
        <div class="d-flex justify-content-between align-items-center h-100">
            <div class="titulo-principal">
                <div class="container p-2">
                    <h4 class="m-0 pl-2"><a href="/" class="text-white abrev">SST</a></h4>
                    <h4 class="m-0 pl-2"><a href="/" class="text-white extendido">Sistema de Seguimiento de Titulación</a></h4>
                </div>
            </div>
            
            <div class="opciones-usuario">
                <ul>
                    <li><a href="#"><span class="oi oi-bell"></span></a></span></li>
                    <li><a href="#"><span class="oi oi-person"></span></a></li>
                    <li><a href="#">| Cerrar Sesión</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="etapas d-none" id="etapas">
        <ul class="position-relative">
            <li class="d-flex align-items-center activo"><a href="{{route('inscripcion-tema.create')}}">Inscripción de tema</a></li>
            <li class="d-flex align-items-center"><a href="#">Designación del comité supervisor</a></li>
            <li class="d-flex align-items-center"><a href="#">Proceso de desarrollo del trabajo de grado</a></li>
            <li class="d-flex align-items-center"><a href="#">Cofirmación del título</a></li>
            <li class="d-flex align-items-center"><a href="#">Designación de tribunal calificador</a></li>
            <li class="d-flex align-items-center"><a href="#">Proceso de revisión del trabajo de grado</a></li>
            <li class="d-flex align-items-center"><a href="#">Entrega del trabajo en formato digital</a></li>
            <li class="d-flex align-items-center"><a href="#">Designación de fecha y hora</a></li>
        </ul>
    </div>
    
    <div class="fondo-oscuro"></div>

    <div class="boton-etapas bg-primary m-0" id="btn-etapas">
        <a href="#" id="etapas-link">
            Etapas <span class="font-weight-bold f-20" id="flecha-mostrar">>></span>
            <span class="font-weight-bold f-20 d-none" id="flecha-ocultar"><<</span>
        </a>
    </div>

    <div class="etapa-actual pb-3 w-100 position-absolute text-sm-center">
        <p class="m-0"><b> Etapa actual:</b> 
            <span class="d-block d-sm-inline">
                <a href="{{route('inscripcion-tema.create')}}" class="text-decoration-none text-secondary">Inscripción de tema</a>
                <span class="oi oi-warning text-warning" data-toggle="tooltip" data-placement="top" title="Falta completar esta etapa"></span> 
            </span>
        </p>
    </div>

    @yield('content')
    
    <footer class="mt-3">
        <div class="container">
            <div class="row footer">
                <div class="col text-center">
                    <small class="texto-footer">&copy; <script>document.write(new Date().getFullYear());</script> IIFHCE | Dev. by Aux. Carlos Ramos S.</small>
                </div>
            </div>
        </div>
    </footer>
    
    <script src="{{asset('jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <script>
        $(function(){
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })

            $(function () {
                $('[data-toggle="popover"]').popover()
            })


            //para mostrar y ocultar el panel de "etapas"
            $('#etapas').addClass('ocultar-etapas');
            $('#etapas').removeClass('d-none');
            $('.fondo-oscuro').hide();

            flagShowEtapas = false;
            $('#btn-etapas').on('click', function(e){
                if(flagShowEtapas == false){ 
                    // panel "etapas" oculto
                    $('#etapas').addClass('mostrar-etapas');
                    $('#etapas').removeClass('ocultar-etapas');
                    $('#flecha-mostrar').addClass('d-none');
                    $('#flecha-ocultar').removeClass('d-none');
                    $(this).addClass('mostrar-btn-etapas');
                    $(this).removeClass('ocultar-btn-etapas');
                    $('.fondo-oscuro').show()
                    flagShowEtapas = true;
                }else{  
                    //panel "etapas" visible
                    $('#etapas').removeClass('mostrar-etapas');
                    $('#etapas').addClass('ocultar-etapas');
                    $('#flecha-mostrar').removeClass('d-none');
                    $('#flecha-ocultar').addClass('d-none');
                    $(this).addClass('ocultar-btn-etapas')
                    $(this).removeClass('mostrar-btn-etapas')
                    $('.fondo-oscuro').hide()
                    flagShowEtapas = false;
                }
            });

        })
    </script>

    @yield('scripts')

</body>
</html>