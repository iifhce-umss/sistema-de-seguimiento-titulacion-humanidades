<?php

namespace App\Http\Controllers;

use App\Http\Requests\InscripcionTema;
use Illuminate\Http\Request;

class InscripcionTemaController extends Controller
{

    private $estudiante;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datos_autocompletados = (Object) [
            "nombre"    => "MERUBIA RODRIGUEZ JIMENA",
            "carrera"    => "Psicología",
            "email"     => "jimena.tam@hotmail.com",
            "celular"   => "77925842",
            "fecha_actual"      => getdate()
        ];
        return view('estudiante.inscripcion-tema')
                ->with('datos', $datos_autocompletados);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InscripcionTema $request)
    {
        dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
