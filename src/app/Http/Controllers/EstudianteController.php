<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EstudianteController extends Controller
{
    private $estudiante;
    
    public function principal() {
        $estudiante = (Object) [
            "nombre"    => "MERUBIA RODRIGUEZ JIMENA",
            "carrera_actual"        => "Psicología",
            "materias_inscritas"    => ["Nombre-mat1", "Nombre-mat2"],
            "email"     => "jimena.tam@hotmail.com",
            "celular"   => "77925842"
        ];

        return view('estudiante.principal')
                ->with('estudiante', $estudiante);
    }

}