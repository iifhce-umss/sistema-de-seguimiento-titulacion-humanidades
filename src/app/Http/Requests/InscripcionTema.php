<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InscripcionTema extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_tesista' => 'required',
            'carrera'        => 'required',
            'direccion'     => 'required',
            'telefono'      => 'required',
            'email'         => 'required',
            'fecha_solicitud' => 'required',
            'linea_investigacion' => 'required',
            'titulo_tema' => 'required',
            'idioma_titulo' => 'required',
            // 'titulo_traducido' => 'required',
            'objetivo_general' => 'required',
            'objetivos_especificos' => 'required',
            'extension_temporal' => 'required',
            'sintesis' => 'required',
        ];
    }
}
